<?php

namespace Aelast\Tools;

class Files
{

    public static function getSortedList($path, $filter = false, $onlydir = false, $onlyfiles = false)
    {
        return self::getList($path, $filter, $onlydir, $onlyfiles);
    }

    // Возвращает список каталогов и файлов в указанной директории $path
    public static function getList($path, $filter = false, $onlydir = false, $onlyfiles = false)
    {
        $directories = $onlyfiles ? [] : self::getDirectoriesList($path);
        $files = $onlydir ? [] : self::getFilesList($path, $filter);
        return array_merge($directories, $files);
    }

    public static function getDirectoriesList($path)
    {
        if (!is_readable($path)) {
            return [];
        }
        $list = [];
        foreach (glob($path . '*', GLOB_ONLYDIR) as $f) {
            if (!is_dir($f) || $f == '.' || $f == '..') {
                continue;
            }
            $dir = static::getDirInfo($f);
            $dir['fullname'] = $f;
            $list[] = $dir;
        }
        return $list;
    }

    public static function getFilesList($path, $filter = false)
    {
        if (!is_readable($path)) {
            return [];
        }
        $list = [];
        foreach (glob($path . '*') as $f) {
            if (!is_file($f)) {
                continue;
            }
            $tmp = static::getFileInfo($f);
            if ($filter && $tmp['type'] != $filter) {
                continue;
            }
            $tmp['fullname'] = $f;
            $list[] = $tmp;
        }
        return $list;
    }

    public static function getDirInfo($path)
    {
        $info = pathinfo($path);
        $info['type'] = 'dir';
        $info['writable'] = is_writable($path);
        return $info;
    }

    public static function getFileInfo($file)
    {
        $info = pathinfo($file);
        $info['writable'] = is_writable($file);
        $info['readable'] = is_readable($file);
        $info['extension_formated'] = isset($info['extension']) ? mb_strtolower($info['extension']) : '';
        $info['size'] = filesize($file);
        $info['size_formated'] = static::formatSize($info['size']);
        $info['type'] = static::getTypeByExt($info['extension_formated']);
        return $info;
    }

    public static function formatSize($size)
    {
        if ($size < 1024) {
            return number_format($size, 0, "", "") . ' bytes';
        } elseif ($size < 1024 * 1024) {
            return number_format(( $size / 1024), 2, ",", "") . ' Kb';
        } elseif ($size < 1024 * 1024 * 1024) {
            return number_format(( $size / 1024 / 1024), 2, ",", "") . ' Mb';
        } elseif ($size < 1024 * 1024 * 1024 * 1024) {
            return number_format(( $size / 1024 / 1024 / 1024), 2, ",", "") . ' Gb';
        } elseif ($size < 1024 * 1024 * 1024 * 1024 * 1024) {
            return number_format(( $size / 1024 / 1024 / 1024 / 1024), 2, ",", "") . ' Tb';
        } else {
            return 'Очень большой файл';
        }
    }

    // Пытаемся получить тип файла
    public static function getTypeByExt($ext)
    {
        switch ($ext) {
            case 'gif': case 'jpg': case 'jpeg': case 'png': case 'bmp':
                return 'image';
            case 'rar': case 'zip': case 'gzip': case 'tar':
                return 'archive';
            case 'pdf':
                return 'pdf';
            case 'txt':
                return 'text';
            case 'mid': case 'wma': case 'mp3':
                return 'audio';
            case 'mov': case 'avi': case 'flv':
                return 'video';
            case 'doc': case 'rtf': case 'docx':
                return 'word';
            case 'xsl': case 'csv': case 'xsls':
                return 'excel';
            case 'fla': case 'swf':
                return 'flash';
            case 'html': case 'htm':
                return 'html';
            case 'php': case 'js':
                return 'script';
            case 'tpl':
                return 'template';
            case 'css':
                return 'style';
            default:
                return 'file';
        }
    }

    // Функция проверяет входит ли путь первого параметра во второй
    // Необходимо для проверки выхода за рамки песочницы
    public function isIncludePath($path1, $path2)
    {
        return strpos(realpath($path1), realpath($path2)) !== false;
    }

    public function getMaxUploadFileSize()
    {
        return min(
                (int) ini_get('upload_max_filesize'), (int) ini_get('post_max_size'), (int) ini_get('memory_limit')
            ) * 1024 * 1024;
    }

}
