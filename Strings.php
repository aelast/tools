<?php

namespace Aelast\Tools;

class Strings
{

    private static $monthes = [
        '1' => 'января',
        '2' => 'февраля',
        '3' => 'марта',
        '4' => 'апреля',
        '5' => 'мая',
        '6' => 'июня',
        '7' => 'июля',
        '8' => 'августа',
        '9' => 'сентября',
        '10' => 'октября',
        '11' => 'ноября',
        '12' => 'декабря'
    ];
    private static $week = [
        '1' => ['full' => 'понедельник', 'short' => 'пн'],
        '2' => ['full' => 'вторник', 'short' => 'вт'],
        '3' => ['full' => 'среда', 'short' => 'ср'],
        '4' => ['full' => 'четверг', 'short' => 'чт'],
        '5' => ['full' => 'пятница', 'short' => 'пт'],
        '6' => ['full' => 'суббота', 'short' => 'сб'],
        '7' => ['full' => 'воскресенье', 'short' => 'вс'],
    ];
    private static $quantitative = [
        'singular' => 'ед. ч.',
        'plural' => 'мн. ч.'
    ];
    private static $cases = [
        'nominative' => 'именительный',
        'genitive' => 'родительный',
        'dative' => 'дательный',
        'accusative' => 'винительный',
        'ablative' => 'творительный',
        'prepositional' => 'предложный'
    ];

    public static function normalizeName($name)
    {
        $segments = explode('_', $name);
        for ($i = 0; $i < count($segments); $i++) {
            $segments[$i] = ucfirst(trim($segments[$i]));
        }
        return implode('', $segments);
    }

    public static function getQuantitative($full = false)
    {
        if ($full) {
            return static::$quantitative;
        }
        return array_keys(static::$quantitative);
    }

    public static function getCases($full = false)
    {
        if ($full) {
            return static::$cases;
        }
        return array_keys(static::$cases);
    }

    public static function translit($string)
    {
        $cyr = [
            "Щ", "Ш", "Ч", "Ц", "Ю", "Я", "Ж", "А", "Б", "В", "Г", "Д", "Е", "Ё", "З", "И", "Й",
            "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ь", "Ы", "Ъ", "Э",
            "щ", "ш", "ч", "ц", "ю", "я", "ж", "а", "б", "в", "г", "д", "е", "ё", "з", "и", "й",
            "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ь", "ы", "ъ", "э"
        ];
        $lat = [
            "Sch", "Sh", "Ch", "C", "Yu", "Ya", "Zh", "A", "B", "V", "G", "D", "E", "E", "Z", "I", "Y",
            "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "H", "'", "Y", "`", "E",
            "sch", "sh", "ch", "c", "yu", "ya", "zh", "a", "b", "v", "g", "d", "e", "e", "z", "i", "y",
            "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "'", "y", "`", "e"
        ];
        for ($i = 0; $i < count($cyr); $i++) {
            $c_cyr = $cyr[$i];
            $c_lat = $lat[$i];
            $string = str_replace($c_cyr, $c_lat, $string);
        }
        return $string;
    }

    public static function UrlTranslit($string)
    {
        $string = static::translit($string);
        $string = strtolower($string);
        $string = preg_replace("/[_\s\.,?!\[\]\/(){}]+/", "-", $string);
        $string = preg_replace("/[^0-9a-z_\-\|]+/", "", $string);
        $string = preg_replace("/y{2,}/", "y", $string);
        $string = preg_replace("/-{2,}/", "-", $string);
        $string = preg_replace("/^-/", "", $string);
        $string = preg_replace("/-$/", "", $string);

        return $string;
    }

    public static function dateFromSql($date)
    {
        if (empty($date)) {
            return null;
        }
        $date_time = explode(' ', $date);
        $time_array = explode(':', $date_time[1]);
        $result = new \stdClass();
        $result->hour = $time_array[0];
        $result->minutes = $time_array[1];
        $result->time = $result->hour . ':' . $result->minutes;
        $date_array = explode('-', $date_time[0]);
        $result->day = $date_array[2];
        $result->month = $date_array[1];
        $result->month_str = static::$monthes[$result->month];
        $result->year = $date_array[0];
        $result->date = $result->day . '.' . $result->month . '.' . $result->year;
        $result->week_day = date('N', mktime(0, 0, 0, $result->month, $result->day, $result->year));
        $result->week_day_str = static::$week[$result->week_day]['full'];
        $result->week_day_short = static::$week[$result->week_day]['short'];
        $result->sql = $date;
        $result->stamp = mktime($result->hour, $result->minutes, 0, $result->month, $result->day, $result->year);
        return $result;
    }

    public static function parseTemplatedString($template, $values)
    {
        if (is_object($values)) {
            $values = get_object_vars($values);
        }
        if (!is_array($values)) {
            return '';
        }
        foreach ($values as $key => $value) {
            if (strpos($template, '{' . $key . '}') !== false) {
                $template = str_replace('{' . $key . '}', $value, $template);
            }
        }
        return $template;
    }

    public static function declension($number, $w1, $w2 = '', $w3 = '')
    {
        $count = $number % 100;
        if (empty($w2)) {
            $w2 = $w1;
        }
        if (empty($w3)) {
            $w3 = $w2;
        }
        if ($count >= 5 && $count <= 20) {
            $result = $w3;
        } else {
            $count = $count % 10;
            if ($count == 1) {
                $result = $w1;
            } elseif ($count >= 2 && $count <= 4) {
                $result = $w2;
            } else {
                $result = $w3;
            }
        }
        return $result;
    }

    public static function escape($string, $type = 'html', $charset = 'UTF-8', $double = true)
    {
        switch ($type) {
            case 'html':
                return htmlspecialchars($string, ENT_QUOTES, $charset, $double);
            case 'htmlall':
                if (function_exists('mb_convert_encoding')) {
                    // mb_convert_encoding ignores htmlspecialchars()
                    $string = htmlspecialchars($string, ENT_QUOTES, $charset, $double);
                    // htmlentities() won't convert everything, so use mb_convert_encoding
                    return mb_convert_encoding($string, 'HTML-ENTITIES', $charset);
                }
                // no MBString fallback
                return htmlentities($string, ENT_QUOTES, $charset, $double);
            case 'url':
                return rawurlencode($string);
            case 'urlpathinfo':
                return str_replace('%2F', '/', rawurlencode($string));
            case 'quotes':
                // escape unescaped single quotes
                return preg_replace("%(?<!\\\\)'%", "\\'", $string);
            case 'javascript':
                // escape quotes and backslashes, newlines, etc.
                return strtr($string, array('\\' => '\\\\', "'" => "\\'", '"' => '\\"', "\r" => '\\r', "\n" => '\\n', '</' => '<\/'));
            default:
                return $string;
        }
    }

}
