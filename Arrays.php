<?php

namespace Aelast\Tools;

class Arrays
{

    public static function merge(array $a, array $b, $replace = true, $ignore_if_leaf_exists = false)
    {
        foreach ($b as $key => $value) {
            if (!$replace && (isset($a[$key]) || array_key_exists($key, $a))) {
                if (is_array($value) && is_array($a[$key])) {
                    $a[$key] = $this->merge($a[$key], $value, $replace);
                    continue;
                }
                if ($ignore_if_leaf_exists) {
                    continue;
                }
            }
            $a[$key] = $value;
        }
        return $a;
    }

    public static function isAssoc($array)
    {
        if (!is_array($array)) {
            return false;
        }
        foreach (array_keys($array) as $key) {
            if (gettype($key) === 'string') {
                return true;
            }
        }
        return false;
    }

}
